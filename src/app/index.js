import './styles/styles.scss';
import 'bootstrap';
import { displayPokeballs, } from './scripts/randomStartersView';
import { firstPokemon, secondPokemon, thirdPokemon } from './scripts/randomStarterDetailApi';
import { displayPokemonList, handleScroll } from './scripts/fullListView';

function addListeners() {
  // TODO
  document.getElementById('submit-btn').addEventListener('click', pokeballListener );
  document.getElementById('full-list-button').addEventListener('click', displayPokemonList);
  document.querySelector('body').onscroll = handleScroll;
}

//localStorage['threeStarters'] = ;

window.onload = function () {
  addListeners();
};

const takeName = () => {
  const trainerName = document.getElementById("trainerName").value;
  return trainerName;
}

const pokeballListener = () => {
  const trainerNameCheck = takeName();
  if (!trainerNameCheck) {
    alert('Enter a valid name, please.')
  } else {
      displayPokeballs();
      document.getElementById('first-pokeball').addEventListener('click', firstPokemon);
      document.getElementById('second-pokeball').addEventListener('click', secondPokemon);
      document.getElementById('third-pokeball').addEventListener('click', thirdPokemon);
  }
};

export {
  takeName
}
