import { randomStarters } from './randomStartersApi';
import { chooseFirstButton, chooseSecondButton, chooseThirdButton } from './randomStarterDetailView';

const randomPokemonInfoContainer = document.getElementById('starter-info');
const threeStarters = randomStarters();
const firstPokemon = () => {
        return threeStarters.then((pokemon) => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon[0].name}`)
        .then((response) => {
            return response.json();
        })
        .then((onePokemonInfo) => {
            const pokemonId = onePokemonInfo.id;
            const pokemonImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png`;
            const pokemonName = onePokemonInfo.name;
            const pokemonType = onePokemonInfo.types.map((type) => type.type.name).join(", ");
            const infoFirstPokemon = `
                <div>
                    <h3 class="main__pokemon-name">${pokemonId} - ${pokemonName}</h3>
                    <p class="main__pokemon-type">Type: ${pokemonType}</p>
                    <img class="main__pokemon-detail-image" src="${pokemonImg}" alt="pokemon-image">
                </div>
                <div> 
                    <button id="choose-first-btn" class="main__submit-button main__choose-pokemon-btn" type="submit">
                        Choose ${pokemonName}
                    </button>
                </div>
            `;
            randomPokemonInfoContainer.innerHTML = infoFirstPokemon;
            document.getElementById('choose-first-btn').addEventListener('click', chooseFirstButton);
        });        
    });    
};
const secondPokemon = () => {
    threeStarters.then((pokemon) => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon[1].name}`)
        .then((response) => {
            return response.json();
        })
        .then((onePokemonInfo) => {
            const pokemonId = onePokemonInfo.id;
            const pokemonImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png`;
            const pokemonName = onePokemonInfo.name;
            const pokemonType = onePokemonInfo.types.map((type) => type.type.name).join(", ");
            const infoFirstPokemon = `
            <div>
                <h3 class="main__pokemon-name">${pokemonId} - ${pokemonName}</h3>
                <p class="main__pokemon-type">Type: ${pokemonType}</p>
                <img class="main__pokemon-detail-image" src="${pokemonImg}" alt="pokemon-image">
            </div>
            <div> 
                <button id="choose-second-btn" class="main__submit-button main__choose-pokemon-btn" type="submit">
                    Choose ${pokemonName}
                </button>
            </div>
            `;
            randomPokemonInfoContainer.innerHTML = infoFirstPokemon;
            document.getElementById('choose-second-btn').addEventListener('click', chooseSecondButton);
        });
    });    
};
const thirdPokemon = () => {
    threeStarters.then((pokemon) => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon[2].name}`)
        .then((response) => {
            return response.json();
        })
        .then((onePokemonInfo) => {
            const pokemonId = onePokemonInfo.id;
            const pokemonImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png`;
            const pokemonName = onePokemonInfo.name;
            const pokemonType = onePokemonInfo.types.map((type) => type.type.name).join(", ");
            const infoFirstPokemon = `
                <div>
                    <h3 class="main__pokemon-name">${pokemonId} - ${pokemonName}</h3>
                    <p class="main__pokemon-type">Type: ${pokemonType}</p>
                    <img class="main__pokemon-detail-image" src="${pokemonImg}" alt="pokemon-image">
                </div>
                <div> 
                    <button id="choose-third-btn" class="main__submit-button main__choose-pokemon-btn" type="submit">
                        Choose ${pokemonName}
                    </button>
                </div>
                
            `;
            randomPokemonInfoContainer.innerHTML = infoFirstPokemon;
            document.getElementById('choose-third-btn').addEventListener('click', chooseThirdButton);
        });
    });    
};

export {
    firstPokemon, 
    secondPokemon,
    thirdPokemon,
}