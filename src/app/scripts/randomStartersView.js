import { randomStarters } from './randomStartersApi';


const displayPokeballs = () => {
    const threeStarters = randomStarters();
    const randomPokemonContainer = document.getElementById('starters');
    
    threeStarters.then((pokemon) => {
        console.log(pokemon);

        
    });
    const showRandomPokemon = `
        <span class="main__starter-pokemon">
            <button class="main__starter-button" id="first-pokeball" type="submit">
                <img class="main__pokeball-starter-img" src="/assets/pokeball.png" alt="pokeball">
            </button>
        </span>
        <span class="main__starter-pokemon">
            <button class="main__starter-button" id="second-pokeball" type="submit">
                <img class="main__pokeball-starter-img" src="/assets/pokeball.png" alt="pokeball">
            </button>
        </span>
        <span class="main__starter-pokemon">
            <button class="main__starter-button" id="third-pokeball" type="submit">
                <img class="main__pokeball-starter-img" src="/assets/pokeball.png" alt="pokeball">
            </button>
        </span>
    `;
    randomPokemonContainer.innerHTML = showRandomPokemon;
};

export {
    displayPokeballs
}



