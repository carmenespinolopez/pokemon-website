import { randomStarters } from './randomStartersApi';
import { takeName } from '../index';

const body = document.getElementById('body');
const subtitle = document.getElementById('subtitle');
const gifLogo1 = document.getElementById('logo-image1');
const gifLogo2 = document.getElementById('logo-image2');
const logo = document.getElementById('logo');
const randomizerSection = document.getElementById('randomizer');
const pokemonSection = document.getElementById('pokemonInfoSection');
const pokemonList = randomStarters();

const chooseFirstButton = async () => {
    const trainerName = takeName();

    pokemonList.then( async (pokemon) => {
        const first = pokemon[0].name;
        fetch(`https://pokeapi.co/api/v2/pokemon/${first}`)
        .then((response) => {
            return response.json();
        })
        .then((firstPokemonInfo) => {
            const pokemonId = firstPokemonInfo.id;
            const pokemonImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png`;
            const pokemonName = firstPokemonInfo.name;
            const pokemonType = firstPokemonInfo.types.map((type) => type.type.name).join(", ");
            const pokemonAbilities = firstPokemonInfo.abilities.map((ability) => ability.ability.name).join(", ");
            const pokemonShape = `<strong>Height</strong>: ${firstPokemonInfo.height} m. - <strong>Weight</strong>: ${firstPokemonInfo.weight} kg.`;
            const teamRocketImage = 'https://smallimg.pngkey.com/png/small/140-1408453_houston-team-rockets-houston-rockets-x-team-rocket.png';

            const infoFirstPokemon = `
                <div class="main__info-chosen-poke">
                    <span>
                        <img class="main__team-rocket-img" src="${teamRocketImage}">
                    </span>
                    <p class="main__pokemon-type">Ha ha ha!</p>
                    <p class="main__pokemon-type">Sorry, ${trainerName}, your Pokémon is ours now but...</p>
                    <p class="main__pokemon-type">You're a loser, I feel sorry for you.</p>
                    <p class="main__pokemon-type">Keep this <span class="capitalize">${pokemonName}</span> we stole from Ash Ketchum:</p>
                    <img class="main__chosen-pokemon-image" src="${pokemonImg}" alt="pokemon-image">
                </div>
                <div class="main__info-chosen-poke margin-bottom">
                    <p><span class="capitalize">${pokemonName}</span> information:</p>
                    <p class="main__pokemon-type"><strong>Type</strong>: ${pokemonType}</p>
                    <p class="main__pokemon-type"><strong>Abilities</strong>: ${pokemonAbilities}</p>
                    <p class="main__pokemon-type">${pokemonShape}</p>
                    <p></p>
                </div>
            `;
            body.classList.add('team-rocket-style');
            logo.src = 'https://cdn.bulbagarden.net/upload/5/5d/Team_Rocket_Logo.png';
            logo.classList.add('header__team-rocket-name');
            subtitle.classList.add('hide');
            gifLogo1.src = 'https://pngimage.net/wp-content/uploads/2018/06/rocket-sprite-png-5.png';
            gifLogo2.src = 'https://pngimage.net/wp-content/uploads/2018/06/rocket-sprite-png-5.png';
            pokemonSection.innerHTML = infoFirstPokemon;
        }); 

        randomizerSection.classList.add('hide');
    })
    
    

};

const chooseSecondButton = async () => {
    const trainerName = takeName();

    pokemonList.then( async (pokemon) => {
        const second = pokemon[1].name;
        fetch(`https://pokeapi.co/api/v2/pokemon/${second}`)
        .then((response) => {
            return response.json();
        })
        .then((secondPokemonInfo) => {
            const pokemonId = secondPokemonInfo.id;
            const pokemonImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png`;
            const pokemonName = secondPokemonInfo.name;
            const pokemonType = secondPokemonInfo.types.map((type) => type.type.name).join(", ");
            const pokemonAbilities = secondPokemonInfo.abilities.map((ability) => ability.ability.name).join(", ");
            const pokemonShape = `<strong>Height</strong>: ${secondPokemonInfo.height} m. - <strong>Weight</strong>: ${secondPokemonInfo.weight} kg.`;
            const teamRocketImage = 'https://smallimg.pngkey.com/png/small/140-1408453_houston-team-rockets-houston-rockets-x-team-rocket.png';

            const infoSecondPokemon = `
                <div class="main__info-chosen-poke">
                    <span>
                        <img class="main__team-rocket-img" src="${teamRocketImage}">
                    </span>
                    <p class="main__pokemon-type">Ha ha ha!</p>
                    <p class="main__pokemon-type">Sorry, ${trainerName}, your Pokémon is ours now but...</p>
                    <p class="main__pokemon-type">You're a loser, I feel sorry for you.</p>
                    <p class="main__pokemon-type">Keep this <span class="capitalize">${pokemonName}</span> we stole from Ash Ketchum:</p>
                    <img class="main__chosen-pokemon-image" src="${pokemonImg}" alt="pokemon-image">
                </div>
                <div class="main__info-chosen-poke margin-bottom">
                    <p><span class="capitalize">${pokemonName}</span> information:</p>
                    <p class="main__pokemon-type"><strong>Type</strong>: ${pokemonType}</p>
                    <p class="main__pokemon-type"><strong>Abilities</strong>: ${pokemonAbilities}</p>
                    <p class="main__pokemon-type">${pokemonShape}</p>
                    <p></p>
                </div>
            `;
            body.classList.add('team-rocket-style');
            logo.src = 'https://cdn.bulbagarden.net/upload/5/5d/Team_Rocket_Logo.png';
            logo.classList.add('header__team-rocket-name');
            subtitle.classList.add('hide');
            gifLogo1.src = 'https://pngimage.net/wp-content/uploads/2018/06/rocket-sprite-png-5.png';
            gifLogo2.src = 'https://pngimage.net/wp-content/uploads/2018/06/rocket-sprite-png-5.png';
            pokemonSection.innerHTML = infoSecondPokemon;
        }); 

        randomizerSection.classList.add('hide');
    })
};

const chooseThirdButton = async () => {
    const trainerName = takeName();

    pokemonList.then( async (pokemon) => {
        const third = pokemon[2].name;
        fetch(`https://pokeapi.co/api/v2/pokemon/${third}`)
        .then((response) => {
            return response.json();
        })
        .then((thirdPokemonInfo) => {
            const pokemonId = thirdPokemonInfo.id;
            const pokemonImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonId}.png`;
            const pokemonName = thirdPokemonInfo.name;
            const pokemonType = thirdPokemonInfo.types.map((type) => type.type.name).join(", ");
            const pokemonAbilities = thirdPokemonInfo.abilities.map((ability) => ability.ability.name).join(", ");
            const pokemonShape = `<strong>Height</strong>: ${thirdPokemonInfo.height} m. - <strong>Weight</strong>: ${thirdPokemonInfo.weight} kg.`;
            const teamRocketImage = 'https://smallimg.pngkey.com/png/small/140-1408453_houston-team-rockets-houston-rockets-x-team-rocket.png';
            
            const infoThirdPokemon = `
                <div class="main__info-chosen-poke">
                    <span>
                        <img class="main__team-rocket-img" src="${teamRocketImage}">
                    </span>
                    <p class="main__pokemon-type">Ha ha ha!</p>
                    <p class="main__pokemon-type">Sorry, ${trainerName}, your Pokémon is ours now but...</p>
                    <p class="main__pokemon-type">You're a loser, I feel sorry for you.</p>
                    <p class="main__pokemon-type">Keep this <span class="capitalize">${pokemonName}</span> we stole from Ash Ketchum:</p>
                    <img class="main__chosen-pokemon-image" src="${pokemonImg}" alt="pokemon-image">
                </div>
                <div class="main__info-chosen-poke margin-bottom">
                    <p><span class="capitalize">${pokemonName}</span> information:</p>
                    <p class="main__pokemon-type"><strong>Type</strong>: ${pokemonType}</p>
                    <p class="main__pokemon-type"><strong>Abilities</strong>: ${pokemonAbilities}</p>
                    <p class="main__pokemon-type">${pokemonShape}</p>
                    <p></p>
                </div>
            `;
            body.classList.add('team-rocket-style');
            logo.src = 'https://cdn.bulbagarden.net/upload/5/5d/Team_Rocket_Logo.png';
            logo.classList.add('header__team-rocket-name');
            subtitle.classList.add('hide');
            gifLogo1.src = 'https://pngimage.net/wp-content/uploads/2018/06/rocket-sprite-png-5.png';
            gifLogo2.src = 'https://pngimage.net/wp-content/uploads/2018/06/rocket-sprite-png-5.png';
            pokemonSection.innerHTML = infoThirdPokemon;
        }); 
        randomizerSection.classList.add('hide');
    })
};

export {
    chooseFirstButton,
    chooseSecondButton,
    chooseThirdButton,
}