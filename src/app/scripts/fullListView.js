import { getFullList } from "./fullListApi";

let offset = 0;

const displayPokemonList = (event, offset = 0) => {
    const pokedex = document.getElementById("full-list-section");
    const randomSection = document.getElementById('randomizer');
    const infoPokemon = document.getElementById('pokemonInfoSection');
    const subtitle = document.getElementById('subtitle');
    const body = document.getElementById('body');
    const gifLogo1 = document.getElementById('logo-image1');
    const gifLogo2 = document.getElementById('logo-image2');
    const logo = document.getElementById('logo');
    randomSection.classList.add('hide');
    infoPokemon.classList.add('hide');
    subtitle.innerHTML = "The complete Pokédex";
    body.classList.remove('team-rocket-style');
    logo.src = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/87044f58-c765-43c5-bc51-8613e3ac7ab1/ddew4m7-c69a2c41-518f-48ca-ba35-8ab1895464e0.png';
    logo.classList.remove('header__team-rocket-name');
    subtitle.classList.remove('hide');
    gifLogo1.src = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/029b8bd9-cb5a-41e4-9c7e-ee516face9bb/dayo3ow-7ac86c31-8b2b-4810-89f2-e6134caf1f2d.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvMDI5YjhiZDktY2I1YS00MWU0LTljN2UtZWU1MTZmYWNlOWJiXC9kYXlvM293LTdhYzg2YzMxLThiMmItNDgxMC04OWYyLWU2MTM0Y2FmMWYyZC5naWYifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.LJBxDkRocQStjZpmj9Injfv73mG2SQZ8X6HNdlP5WHw';
    gifLogo2.src = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/029b8bd9-cb5a-41e4-9c7e-ee516face9bb/dayo3ow-7ac86c31-8b2b-4810-89f2-e6134caf1f2d.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvMDI5YjhiZDktY2I1YS00MWU0LTljN2UtZWU1MTZmYWNlOWJiXC9kYXlvM293LTdhYzg2YzMxLThiMmItNDgxMC04OWYyLWU2MTM0Y2FmMWYyZC5naWYifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.LJBxDkRocQStjZpmj9Injfv73mG2SQZ8X6HNdlP5WHw';

    if (offset === 0) {
        pokedex.innerHTML = ""; 
    }
    const pokemonListPromise = getFullList(offset);
    pokemonListPromise.then((pokemonList) => {
    pokemonList.forEach((pokemon) => {
        const div = document.createElement('div');
        div.classList.add('main__full-list-items');
        const anchor = document.createElement('a');
        anchor.classList.add('link');
        const url = `https://www.wikidex.net/wiki/${pokemon.name}`;
        anchor.href = url;
        div.appendChild(anchor);

        switch (pokemon.name) {
            case 'bulbasaur':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Grass starter</p>
                `;
                div.classList.add('main__full-list-items--green');
                break;
            case 'charmander':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Fire starter</p>
                `;
                div.classList.add('main__full-list-items--red');
                break;
            case 'squirtle':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Water starter</p>
                `;
                div.classList.add('main__full-list-items--blue');
                break;
            case 'articuno':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Ice/Flying legendary</p>
                `;
                div.classList.add('main__full-list-items--blue');
                break;
            case 'zapdos':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Electric/Flying legendary</p>
                `;
                div.classList.add('main__full-list-items--yellow');
                break;
            case 'moltres':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Electric/Flying legendary</p>
                `;
                div.classList.add('main__full-list-items--red');
                break;
            case 'dragonite':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Dragon/Flying pseudo-legendary</p>
                `;
                div.classList.add('main__full-list-items--orange');
                break;
            case 'mewtwo':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Phychic-type legendary</p>
                `;
                div.classList.add('main__full-list-items--purple');
                break;
            case 'mew':
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                    <p>Psychic-type mythical</p>
                `;
                div.classList.add('main__full-list-items--pink');
                break;
            case 'chikorita':
                div.classList.add('hide');
                break;
            case 'bayleef':
                div.classList.add('hide');
                break;
            case 'meganium':
                div.classList.add('hide');
                break;
            default:
                anchor.innerHTML = `
                    <h3># ${pokemon.id}</h3>
                    <h3 class="capitalize">${pokemon.name}</h3>
                `;
        }
        pokedex.appendChild(div);

    });
    });
};

const handleScroll = (event) => {
    if (Math.ceil(window.scrollY) == Math.ceil(document.querySelector('body').scrollHeight - window.innerHeight)) {
      offset += 31;
      if (document.getElementById('full-list-section').childElementCount > 1 && offset < 151) {
        displayPokemonList(event, offset);
      }
    }
  }
  
//chikorita, bayleef meganium

export { 
    displayPokemonList,
    handleScroll,
};