const getFullList = async (offset) => {
  const url = `https://pokeapi.co/api/v2/pokemon?limit=30&offset=${offset}`;
  const res = await fetch(url).catch((error) => {
    throw new Error("Error");
  });

  const { results: data } = await res.json();
  const getIdFromUrl = (url) => url.split("/").slice(-2).join("");
  
  const pokemonList = data.map(({ name, url }) => ({
    name,
    id: getIdFromUrl(url),
  }));

  return pokemonList;
};

class Pokemon {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

const bulbasaur = new Pokemon(1, "bulbasaur");

export { getFullList };
