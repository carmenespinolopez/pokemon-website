const randomStarters = async () => {
    const url = "https://pokeapi.co/api/v2/pokemon/?offset=20&limit=151";

    const res = await fetch(url)
    .catch((error) => console.error('No hay Pokémon salvajes.')
    );
    
    const { results: data } = await res.json();
    const pokemonList = data.map(({ name }) => ({ name }));

    randomNums();
    let threeStarters = [];
    let num = 0;
    for (let i = 0; i < pokemonList.length; i++) {
        if (i === randomNumList[0] ||i === randomNumList[1] || i === randomNumList[2]) {
            threeStarters[num] = pokemonList[i];
            num++;
        }
    };
    return threeStarters;
};

let randomNumList = [];
const randomNums = () => {
  let num;
  for (let i = 0; i < 3; i++) {
    num = Math.floor(Math.random() * 151);
    randomNumList[i] = num;
  }
};

export { randomStarters };


